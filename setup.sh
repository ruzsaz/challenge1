#!/usr/bin/env bash
set -x
set -e
which easy_install || (curl https://bootstrap.pypa.io/ez_setup.py >ez_setup.py; python ez_setup.py)
which pip || easy_install pip
which virtualenv || pip install virtualenv
[ -d env ] || virtualenv --python=python2.7 env
. env/bin/activate
pip install -r requirements.txt
python manage.py migrate
