# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('graph', '0002_auto_20160418_1047'),
    ]

    operations = [
        migrations.AlterField(
            model_name='edge',
            name='category',
            field=models.CharField(max_length=16, db_index=True),
        ),
        migrations.AlterField(
            model_name='edge',
            name='source',
            field=models.CharField(max_length=32, db_index=True),
        ),
        migrations.AlterField(
            model_name='edge',
            name='target',
            field=models.CharField(max_length=32, db_index=True),
        ),
    ]
