# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('graph', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Node',
            fields=[
                ('name', models.CharField(max_length=32, serialize=False, primary_key=True)),
                ('url', models.CharField(max_length=256, null=True)),
                ('data', models.TextField(null=True)),
            ],
        ),
        migrations.AddField(
            model_name='edge',
            name='category',
            field=models.CharField(default='valami', max_length=16),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='edge',
            name='data',
            field=models.TextField(null=True),
        ),
    ]
