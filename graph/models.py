from django.db import models


class Node(models.Model):
    name = models.CharField(max_length=32, null=False, primary_key=True)
    url  = models.CharField(max_length=256, null=True)
    data = models.TextField(null=True)


class Edge(models.Model):
    source   = models.CharField(max_length=32, null=False, db_index=True)
    target   = models.CharField(max_length=32, null=False, db_index=True)
    category = models.CharField(max_length=16, null=False, db_index=True)
    data     = models.TextField(null=True)
