from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from graph import models


def index(request):
    return HttpResponse("Hello world")


def jsonapi(request):
    nodes = [ {"name": x.name, "url": x.url} for x in models.Node.objects.all() ]
    edges = [ {"source": x.source, "target": x.target, "category": x.category} for x in models.Edge.objects.all() ]
    return JsonResponse({"nodes": nodes, "edges": edges})


