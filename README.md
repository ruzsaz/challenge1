# Developer applicant assignment

## Data collection phase.
Find as many connections between you and Maven7 as you can! Follow your connections (and their connections (and their connections [...])) on social media sites, networking portals, professional networking tools, etc. and track us down. Technically speaking: build a graph which consists of one component, where nodes are actual people or event or project or repo or anything else people are involved in, and edges are meaningful connections between these. Put labels on the nodes: a name or other id string, and an url. Put category labels on the edges, e.g. friend-of, participated-in. There is no restriction on how you gather the data or how much should it be.
(Disclaimer: only include data you have proper access right to; respect the rights of other people and companies.)

## Implementation phase.
Check out this repo on a linux or mac.
Run setup.sh. It will install a virtualenv and set up django. You need to activate the virtualenv to run the site or use its commands. You have a working website on your localhost by running manage.py runserver. It has a simple api endpoint: /graph, which returns a json representation of the graph in the database.
You can import your graph into the database by piping your file into the manage.py loadgraph command.
Put Angular.js into the page, and create two menu items: one should show the list of nodes in the graph, the other should visualize the graph. For the visualization you can use d3.js or cytoscape.js or any other library you wish. Show labels for the nodes and show all data you found in the json from the api.
Make it compelling and fun to use, make it easy for us to understand how are we connected!

## Delivery.
Push your data and code to a fork of the repository. Choose a Maven7 employee from your graph whose competency may be appropriate to check your solution, and send an email to this person with the link to your repo.

Have fun!